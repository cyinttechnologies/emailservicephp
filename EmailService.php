<?php

/*EmailService.php
Swiftmailer symfony service to send emails
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


namespace CYINT\ComponentsPHP\Services;
use Doctrine\Bundle\DoctrineBundle\Registry;
use CYINT\ComponentsPHP\Bundles\SettingsBundle\Entity\Setting as Setting;

class EmailService
{
    protected $SwiftMailer = null;
    protected $Transport = null;
    protected $Templating = null;
    protected $message = null;
    protected $from = null;
    protected $from_emmail = null;
    protected $support_email = null;
    protected $Doctrine = null;
    protected $host = null;
    protected $user = null;
    protected $password = null;
    protected $port = null;
    protected $constructing = false;

    function __construct($SwiftMail, $Templating, Registry $Doctrine)
    {  
        $this->constructing = true;
        $Transport = \Swift_SmtpTransport::newInstance();
        $SwiftMail = \Swift_Mailer::newInstance($Transport);
        $this->setSwiftmailer($SwiftMail);
        $this->setTransport($Transport);
        $this->setTemplating($Templating);
        $this->setDoctrine($Doctrine);
        $smtp = $Doctrine->getRepository('CYINTSettingsBundle:Setting')->findByNamespace('smtp');
        $this->setHost($smtp['host']);
        $this->setPort($smtp['port']);
        $this->setFrom($smtp['from']);
        $this->setFromEmail($smtp['email']);
        $this->setUser($smtp['user']);
        $this->setPassword($smtp['password']);

        $this->setTransport($SwiftMail->getTransport());
        $this->setSupportEmail($smtp['support']);
        $this->constructing = false;
        $this->configureSwiftMail(); 
    }

    public function configureSwiftMail()
    {
        if(!$this->constructing)
            $this->message = \Swift_Message::newInstance();
    }

    public function sendForgotPasswordEmail($to, $token, $subject = 'Forget your password?')
    {
        $message = $this->message
            ->setSubject($subject)
            ->setFrom($this->support_email)
            ->setTo($to)
            ->setBody(
                $this->Templating->render(
                    // app/Resources/views/Emails/registration.html.twig
                    'email/forgot.html.twig',
                    array('token' => $token)
                ),
                'text/html'
            )
        ;
        $this->getSwiftMailer()->send($message);        

    }

    public function sendErrorNotification($Transaction, $Machine, $error)
    {
        $message = $this->message
            ->setSubject('Error: Kiosk ' . $Machine->getId())
            ->setFrom($this->getFromEmail())
            ->setTo($this->support_email)
            ->setBody(
                $this->Templating->render(
                    // app/Resources/views/Emails/registration.html.twig
                    'email/error.html.twig',                   
                    array(
                        'Transaction' => $Transaction->toArray()
                        ,'Machine' => $Machine->toArray()
                        ,'error' => $error
                    )
                ),
                'text/html'
            )
        ;
        $this->getSwiftMailer()->send($message);        
    }

    public function setDoctrine(Registry $Doctrine)
    {
        $this->Doctrine = $Doctrine;
    }

    public function getDoctrine()
    {   
        return $this->Doctrine;
    }   

    public function getTransport()
    {
        return $this->Transport;
    }

    public function setTransport($Transport)
    {
        $this->Transport = $Transport;
        return $this;
    }

    public function setHost($host)
    {
        $this->host = $host;
        $Transport = $this->getTransport();
        $Transport->setHost($host);       
        $this->configureSwiftMail(); 
    }

    public function getHost()
    {
        return $this->host;        
    }

    public function setUser($user)
    {
        $this->user = $user;      
        $Transport = $this->getTransport();
        $Transport->setUserName($user);       
        $this->configureSwiftMail(); 
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setPassword($password)
    {
        $this->password = $password;      
        $Transport = $this->getTransport();
        $Transport->setPassword($password);        
        $this->configureSwiftMail(); 
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPort($port)
    {
        $this->port = $port;      
        $Transport = $this->getTransport();
        $Transport->setPort($port);  
        $this->configureSwiftMail();     
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setFrom($from)
    {
        $this->from = $from;      
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setSupportEmail($support_email)
    {
        $this->support_email = $support_email;      
    }

    public function getSupportEmail()
    {
        return $this->support_email;
    }

    public function setTemplating($Templating)
    {
        $this->Templating = $Templating;
    }

    public function getTemplating()
    {
        return $this->Templating;
    }

    public function setSwiftMailer($SwiftMail)
    {
        $this->SwiftMailer = $SwiftMail;
    }

    public function getSwiftMailer()
    {
        return $this->SwiftMailer;
    }

    public function getFromEmail()
    {
        return $this->from_email;
    }

    public function setFromEmail($from_email)
    {
        $this->from_email = $from_email;
    }
}

?>
